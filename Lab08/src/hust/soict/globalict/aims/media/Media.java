package hust.soict.globalict.aims.media;

public abstract class Media implements Comparable{
	public int id;
    private String title;
    private String category;
    private float cost;

    
    public Media() {

	}

	public Media(String title, String category, float cost) {
		this.title = title;
		this.category = category;
		this.cost = cost;
	}

    public String getTitle() {
		return title;
	}

	public String getCategory() {
		return category;
	}

    public float getCost() {
		return cost;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof Media)) {
            return false;
        }

        final Media media = (Media) obj;
        if (this.id != media.id) {
            return false;
        }

        return true;
    }
	@Override
	public int compareTo(Object obj) {
		// TODO Auto-generated method stub
		if (obj == null) {
            return -1;
        }

        if (!(obj instanceof Media)) {
            return -1;
        }
        final Media media = (Media) obj;
        if ((this.title == null) ? (media.title != null) : !this.title.equals(media.title)) {
            return this.title.compareTo(media.title);
        }
        if (this.id != media.id) {
            return this.id>media.id ? 1 : -1;
        }
		return 0;
	}
}
