
public class ArraySort {
	
	public static void sort(int[] array){
		int temp;
		for(int i = 0; i < array.length - 1; i++) {
			for(int j = i + 1; j < array.length; j++) {
				if(array[i] >= array[j]) {
					temp = array[j];
					array[j] = array[i];
					array[i] = temp;
				}else
					continue;
			}
		}
	}
	
	public static void print(int[] array) {
		for(int i = 0; i < array.length; i++) {
			System.out.print(array[i] + "  ");
		}
	}
	
	public static void main(String[] args) {
		int[] array1 = {1789, 2035, 1899, 1456, 2013}; //input constant array
		sort(array1);
		System.out.println("Array after sorting:");
		print(array1);
	}

}
