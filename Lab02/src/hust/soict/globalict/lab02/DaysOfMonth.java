import java.util.Scanner;

public class DaysOfMonth {
	
	public static void main(String[] args) {
		int  year;
		String month;
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter the month: ");
		month = sc.next();
		int monthInt = MonthProcess(month);
		while(monthInt == 0) {
		System.out.println("Invalid month. Enter the month: ");
		month = sc.next();
		monthInt = MonthProcess(month);
		}
		
		System.out.println("Enter the year: ");
		year = sc.nextInt();
		while(year < 1) {
			System.out.println("Invalid year. Enter the year: ");
			year = sc.nextInt();
		}
		
		print(monthInt, year);
	}
	public static boolean isLeapYear(int year) {
		if((year % 4 == 0 && year % 100 != 0) || (year % 4 == 0 && year % 400 == 0))
			return true;
		else 
			return false;
	}
	
	public static int MonthProcess(String month) {
		int monthInt;
		if(month.equals("January")||month.equals("Jan.")||month.equals("Jan")||month.equals("1"))
			monthInt = 1;
		else if(month.equals("March")||month.equals("Mar.")||month.equals("Mar")||month.equals("3")) 
			 monthInt = 3;
		 else if(month.equals("May")||month.equals("5"))
			 monthInt = 5;
		 else if(month.equals("July")||month.equals("Jul")||month.equals("7"))
			 monthInt = 7;
		 else if(month.equals("August")||month.equals("Aug.")||month.equals("Aug")||month.equals("8"))
			 monthInt = 8;
		 else if(month.equals("October")||month.equals("Oct.")||month.equals("Oct")||month.equals("10"))
			 monthInt = 10;
		 else if(month.equals("December")||month.equals("Dec.")||month.equals("Dec")||month.equals("12"))
			 monthInt = 12;
		 else if(month.equals("February")||month.equals("Feb.")||month.equals("Feb")||month.equals("2")) 
			 monthInt = 2;
		 else if(month.equals("April")||month.equals("Apr.")||month.equals("Apr")||month.equals("4"))
			 monthInt = 4;
		 else if(month.equals("June")||month.equals("June")||month.equals("6"))
			 monthInt = 6;
		 else if(month.equals("September")||month.equals("Sep.")||month.equals("Sep")||month.equals("9"))
			 monthInt = 9;
		 else if(month.equals("November")||month.equals("Nov.")||month.equals("Nov")||month.equals("11"))
			 monthInt = 11;
		 else
			 monthInt = 0;
		return monthInt;
	}
	
	public static void print(int monthInt, int year) {
		if(monthInt == 1 || monthInt == 3||monthInt == 5||monthInt == 7||monthInt == 8||monthInt == 10||monthInt == 12)
			System.out.println("Month has 31 days");
		else if(monthInt == 4|| monthInt == 6||monthInt == 9||monthInt == 11)
			System.out.println("Month has 30 days");
		else if (monthInt == 2){
			if(isLeapYear(year) == true)
				System.out.println("Month has 29 days");
			else
				System.out.println("Month has 28 days");
		}
	}
	
}
