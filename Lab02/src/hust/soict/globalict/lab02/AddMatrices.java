import java.util.Scanner; 

public class AddMatrices {
	public static void main(String[] args) {
		
	Scanner sc = new Scanner(System.in);
	System.out.print("Enter number of rows and columns of the matrix: ");
	int row = sc.nextInt();
	int col = sc.nextInt();
	int[][] a1 = new int[row][col];
	int[][] a2 = new int[row][col];
	
	System.out.println("Enter the first matrix: ");
	for(int i = 0; i < row; i++) {
		for(int j = 0; j < col; j++) {
			a1[i][j] = sc.nextInt();
	}
}

	System.out.println("Enter the second matrix: ");
	for(int i = 0; i < row; i++) {
		for(int j = 0; j < col; j++) {
			a2[i][j] = sc.nextInt();
	}
}
	System.out.println("Sum of the 2 matrices: ");
	for(int i = 0; i < row; i++) {
		for(int j = 0; j < col; j++) {
			System.out.print((a1[i][j] + a2[i][j]) + " ");
	}
		System.out.print("\n");
}
}
}