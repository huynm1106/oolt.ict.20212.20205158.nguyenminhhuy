public class DateTest {

	public static void main(String[] args) {
		//Print out the current date
		MyDate test1= new MyDate();
		test1.printline();
		
		//Print out the date input
		MyDate test2= new MyDate(1,1,2020);
		test2.printline();
		
		//Print out the date input
		MyDate test3= new MyDate("February 18 2019");
		test3.printline();
		
		// Input June 11 2002 from keyboard to test accept method
		MyDate test4 = new MyDate();
		test4 = test4.accept(); 
		test4.printline();
		
		
		test2.print();
		test2.printFormat();
		
		DateUtils.dateCompare(test3, test3);   ///test comparing two dates
		DateUtils.dateSort(test1,test2,test3,test4);    ///test sorting a number of dates
	}
}
