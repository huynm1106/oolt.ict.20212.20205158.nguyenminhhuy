package hust.soict.globalict.lab01;
import java.util.Scanner;
import java.lang.Math;

public class Equation {
	private static void firstDegreeOneVariable(){
		System.out.println("Enter coefficients a and b respectively:");
		Scanner sc = new Scanner(System.in);
		double a = sc.nextDouble();
		double b = sc.nextDouble();
		if(a != 0)
			System.out.println("The equation has a unique solution x = " + (-b/a));
		else 
			System.out.println("Error! Coefficient a cannot be 0");
}
	
	private static void firstDegreeTwoVariable() {
		System.out.println("Enter coefficients a_11 a_12 b_1 a_21 a_22 b_2 respectively:");
		Scanner sc = new Scanner(System.in);
	
		double a11 = sc.nextDouble();
		double a12 = sc.nextDouble();
		double b1 = sc.nextDouble();
		double a21 = sc.nextDouble();
		double a22 = sc.nextDouble();
		double b2 = sc.nextDouble();
		
		double d = a11*a22 - a21*a12;
		double d1 = b1*a22 - b2*a12;
		double d2 = a11*b2 - a21*b1;
		if(d != 0)
			System.out.println("The system has a unique solution x_1, x_2: " + d1/d + " " + d2/d);
		else {
			if(d1 == 0 && d2 == 0)
				System.out.println("The system has infinitely many solutions");
			else
				System.out.println("The system has no solution");
		}
			
	}
	
	private static void secondDegreeOneVariable() {
		System.out.println("Enter coefficients a b c respectively:");
		Scanner sc = new Scanner(System.in);
		double a = sc.nextDouble();
		double b = sc.nextDouble();
		double c = sc.nextDouble();
		double delta = b*b - 4*a*c;
		if(a == 0)
			System.out.println("Error! Coefficient a cannot be 0");
		else {
			if(delta == 0)
				System.out.println("The equation has double root x = " + (-b/(2*a)));
			else if(delta > 0)
				System.out.println("The equation has two distinct roots x1 = " + 
			(-b + Math.sqrt(delta))/(2*a)+ " x2 = " + (-b - Math.sqrt(delta))/(2*a));
			else 
				System.out.println("The equation has no solution");
		}
	}
	
	public static void main(String[] args) {
		while(true){
		System.out.println("1. The first-degree equation with one variable");
		System.out.println("2. The first-degree equation with two variable");
		System.out.println("3. The second-degree equation with one variable");
		System.out.println("4. Exit");
		System.out.println("Choose a number");
		Scanner sc = new Scanner(System.in);
		int n= sc.nextInt();
		switch(n) {
		case 1:
			firstDegreeOneVariable();
			break;
		case 2:
			firstDegreeTwoVariable();
			break;
		case 3: 
			secondDegreeOneVariable();
			break;
		case 4:
			System.exit(0);
		}
		
	}
}
}
