package hust.soict.globalict.lab01;
import java.util.Scanner;	


public class Result {
	
	public static double sum(double num1, double num2) {
		return num1 + num2;
	}
	public static double difference(double num1, double num2) {
		return num1>num2?(num1-num2):(num2-num1);
	}
	public static double product(double num1, double num2) {
		return num1*num2;
	}
	public static void quotient(double num1, double num2) {
		if(num2 != 0)
		System.out.println("Quotient: " + (num1/num2));
		else 
		System.out.println("Error! The divisor cannot be 0");
	}
	
	public static void main(String args[]) {
		double num1, num2;
		System.out.println("Enter two double numbers: ");
		Scanner sc = new Scanner(System.in);
		num1 = sc.nextDouble();
		num2 = sc.nextDouble();
		System.out.println("Sum: " + sum(num1,num2));
		System.out.println("Difference: " + difference(num1,num2));
		System.out.println("Product: " + product(num1,num2));
		quotient(num1,num2);
	}
}


