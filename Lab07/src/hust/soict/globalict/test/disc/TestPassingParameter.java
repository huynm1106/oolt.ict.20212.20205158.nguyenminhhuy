package hust.soict.globalict.test.disc;
import hust.soict.globalict.aims.media.DigitalVideoDisc;

//done
public class TestPassingParameter {

	public static void main(String[] args) {
		DigitalVideoDisc jungleDVD = new DigitalVideoDisc("Jungle", "Action", "James Gunn", 120, 17000);
		DigitalVideoDisc cinderellaDVD = new DigitalVideoDisc("Cinderella", "Romance", "Disney", 90, 5000);
		
		swap(jungleDVD, cinderellaDVD);			//old swap
		System.out.println("jungle dvd title: " + jungleDVD.getTitle());
		System.out.println("cinderella dvd title: " + cinderellaDVD.getTitle());
		
		//changeTitle(jungleDVD, cinderellaDVD.getTitle());   
		correctSwap(jungleDVD, cinderellaDVD);
		System.out.println("jungle dvd title, category, director, length, cost: ");
		System.out.println(jungleDVD.getTitle());
		System.out.println(jungleDVD.getCategory());
		System.out.println(jungleDVD.getDirector());
		System.out.println(jungleDVD.getLength());
		System.out.println(jungleDVD.getCost());
		System.out.println("cinderella dvd title, category, director, length, cost: ");
		System.out.println(cinderellaDVD.getTitle());
		System.out.println(cinderellaDVD.getCategory());
		System.out.println(cinderellaDVD.getDirector());
		System.out.println(cinderellaDVD.getLength());
		System.out.println(cinderellaDVD.getCost());
		
	}
	public static void swap(Object o1, Object o2) {
		Object tmp = o1;
		o1 = o2;
		o2 = tmp;
	}
	
	public static void changeTitle(DigitalVideoDisc dvd, String title) {
		String oldTitle = dvd.getTitle();
		dvd.setTitle(title);
		dvd = new DigitalVideoDisc(oldTitle);
	}

	
	
	public static void correctSwap(DigitalVideoDisc dvd1, DigitalVideoDisc dvd2) {
		String oldTitle = dvd1.getTitle();
		String oldCategory = dvd1.getCategory();
		String oldDirector = dvd1.getDirector();
		int oldLength = dvd1.getLength();
		float oldCost = dvd1.getCost();
		dvd1.setTitle(dvd2.getTitle());
		dvd1.setCategory(dvd2.getCategory());
		dvd1.setDirector(dvd2.getDirector());
		dvd1.setLength(dvd2.getLength());
		dvd1.setCost(dvd2.getCost());
		//dvd1 = new DigitalVideoDisc(oldTitle, oldCategory, oldDirector, oldLength, oldCost);
		dvd2.setTitle(oldTitle);
		dvd2.setCategory(oldCategory);
		dvd2.setDirector(oldDirector);
		dvd2.setLength(oldLength);
		dvd2.setCost(oldCost);
}
}
