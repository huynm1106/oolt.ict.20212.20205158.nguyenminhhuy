package hust.soict.globalict.aims.media;

import java.util.ArrayList;

public class CompactDisc extends Disc implements Playable{
	private String artist;
	private ArrayList<Track> tracks = new ArrayList<Track>();

	public String getArtist() {
		return artist;
	}

	public CompactDisc() {
	}

	public CompactDisc(String title, String category, float cost, String artist) {
		super(title, category, cost);
		this.artist = artist;
	}
	
	public void addTrack(Track track) {
		if(tracks.contains(track)) {
			System.out.println("The input track is already in the list of tracks");
		} else {
			tracks.add(track);
			System.out.println("Track has been added");
		}
	}
	
	public void removeTrack(Track track) {
		if(tracks.contains(track)) {
			tracks.remove(track);
			System.out.println("Track has been removed");
		} else {
			System.out.println("The input track does not exist!");
		}
	}
	
	public int getLength() {
		int sum = 0;
		for(int i = 0; i < tracks.size(); i++) {
			sum += tracks.get(i).getLength();
		}
		return sum;
	}
	
	
	@Override
	public void play() {
		System.out.println("CD Tracks display: ");
		for(int i = 0; i < tracks.size(); i++) {
			System.out.println("Track " + (i+1) +" :");
			tracks.get(i).play();
		}
	}
}
