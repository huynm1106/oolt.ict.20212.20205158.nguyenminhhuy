package hust.soict.globalict.aims.media;

public class Disc extends Media{
	
	private int length;
	private String director;
	
	public int getLength() {
		return length;
	}

	public String getDirector() {
		return director;
	}

	public Disc() {
		super();
	}
	public Disc(String title) {
        super(title);
    }

    public Disc(String title, String category) {
        super(title, category);
    }

    public Disc(String title, String category, String director) {
        super(title, category);
        this.director = director;
    }

    public Disc(String title, String category, String director, int length) {
        super(title, category);
        this.length = length;
        this.director = director;
    }

    public Disc(String title, String category, float cost, String director, int length) {
        super(title, category, cost);
        this.length = length;
        this.director = director;
    }

	public Disc(String title, String category, float cost) {
		super(title, category, cost);
	}

}
