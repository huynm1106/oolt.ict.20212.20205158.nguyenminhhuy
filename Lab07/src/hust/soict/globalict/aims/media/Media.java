package hust.soict.globalict.aims.media;

public abstract class Media {
	private String title;
	private String category;
	private float cost;
	private int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Media(String title) {
		super();
		this.title = title;
	}

	public Media(String title, String category) {
		super();
		this.title = title;
		this.category = category;
	}

	public String getTitle() {
		return title;
	}

//	public void setTitle(String title) {
//		this.title = title;
//	}

	public String getCategory() {
		return category;
	}

//	public void setCategory(String category) {
//		this.category = category;
//	}

	public float getCost() {
		return cost;
	}

//	public void setCost(float cost) {
//		this.cost = cost;
//	}

	public Media() {
	}

	public Media(String title, String category, float cost) {
		this.title = title;
		this.category = category;
		this.cost = cost;
	}

}
