package hust.soict.globalict.aims.media;

import hust.soict.globalict.aims.utils.MyDate;

public class DigitalVideoDisc extends Disc implements Playable{

	private String director;
	private int length;
	private MyDate dateOrdered;

	public DigitalVideoDisc() {

	}

	public DigitalVideoDisc(String title) {
		super(title);
	}

	public DigitalVideoDisc(String title, String category) {
		super(title, category);
	}

	public DigitalVideoDisc(String title, String category, String director) {
		super(title, category);
		this.director = director;
	}

	public DigitalVideoDisc(String title, String category, String director, int length, float cost) {
		super(title, category);
		this.director = director;
		this.length = length;
		this.setCost(cost);
		;
	}

	public boolean search(String title) {
		String[] parts = title.split(" ");
		for (int i = 0; i < parts.length; i++) {
			if (this.getTitle().toUpperCase().contains(parts[i].toUpperCase()) == false)
				return false;
		}
		return true;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public MyDate getDateOrdered() {
		return dateOrdered;
	}

	public void setDateOrdered(MyDate dateOrdered) {
		this.dateOrdered = dateOrdered;
	}
	
	@Override
	public void play() {
		System.out.println("Playing DVD: " + this.getTitle());
		System.out.println("DVD length: " + this.getLength());
	}
}