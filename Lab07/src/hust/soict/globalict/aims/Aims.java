package hust.soict.globalict.aims;
import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.media.Media;
import hust.soict.globalict.aims.order.Order;

import java.util.ArrayList;
import java.util.Scanner;



public class Aims {
	public static void showMenu() {
		System.out.println("Order Management Application: ");
		System.out.println("--------------------------------");
		System.out.println("1. Create new order");
		System.out.println("2. Add item to the order");
		System.out.println("3. Delete item by id");
		System.out.println("4. Display the items list of order");
		System.out.println("0. Exit");
		System.out.println("--------------------------------");
		System.out.println("Please choose a number: 0-1-2-3-4");
	}

//	public static DigitalVideoDisc deleteItemById( int id,Media media) {
//		for( Media m:media) {
//			if(m.getId() == id) {
//				return dvd.get(i);
//			}
//		}
//		
//		for (DigitalVideoDisc m : dvd) {
//            if(m.getId() == id){
//            	return m;
//            }
//        }
//	}
	
	public static void main(String[] args){
		int choice;
		Order anOrder1 = new Order();
		DigitalVideoDisc[] dvd = new DigitalVideoDisc[3];
		
		do {
			showMenu();
			Scanner sc = new Scanner(System.in);
			choice = sc.nextInt();
		
		switch(choice) {
		case 0:
			System.exit(0);
			return;
		case 1:
			System.out.println("Order is created!");
			break;
		case 2:
//			System.out.println("Input the number of items you want to add");
//			//Scanner sc = new Scanner(System.in);
//			int totalItem = sc.nextInt();
//			for( int i = 0 ; i < totalItem; i++) {
//				System.out.println("Input name: ");
//				dvd.get(i).setTitle(sc.next());
//				System.out.println("Input category: ");
//				dvd.get(i).setCategory(sc.next());
//				System.out.println("Input cost: ");
//				dvd.get(i).setCost(sc.nextFloat());
//				System.out.println("Input director: ");
//				dvd.get(i).setDirector(sc.next());
//				System.out.println("Input length: ");
//				dvd.get(i).setLength(sc.nextInt());
//				System.out.println("Input id: ");
//				dvd.get(i).setId(sc.nextInt());
//				anOrder1.addMedia(dvd.get(i));
//			}
//test chuc nang 2 add item 
			DigitalVideoDisc dvd0 = new DigitalVideoDisc("The Lion King");
			dvd0.setCategory("Animation");
			dvd0.setCost(19.95f);
			dvd0.setDirector("Roger Allers");
			dvd0.setLength(87);
			dvd0.setId(1);
			dvd[0] = dvd0;
			anOrder1.addMedia(dvd0);

			
			DigitalVideoDisc dvd1 = new DigitalVideoDisc("Star Wars");
			dvd1.setTitle("Star Wars");
			dvd1.setCategory("Science Fiction");
			dvd1.setCost(24.95f);
			dvd1.setDirector("George Lucas");
			dvd1.setLength(124);
			dvd1.setId(2);
			dvd[1] = dvd1;
			anOrder1.addMedia(dvd1);
			
			DigitalVideoDisc dvd2 = new DigitalVideoDisc("Aladdin");
			dvd2.setTitle("Aladdin");
			dvd2.setCategory("Animation");
			dvd2.setCost(18.99f);
			dvd2.setDirector("John Musker");
			dvd2.setLength(90);
			dvd2.setId(3);
			dvd[2] = dvd2;
			anOrder1.addMedia(dvd2);
			break;
		case 3:
			System.out.println("Input the id you want to delete:");
			anOrder1.removeMedia(sc.nextInt(),dvd);
			break;
		case 4:
			anOrder1.printOrder();
			break;
		}
		}while(choice != 0);
//		Order anOrder1 = new Order();
//		////DigitalVideoDisc[] dvd = new DigitalVideoDisc[3]; //test for adding list parameter
//		
//		DigitalVideoDisc dvd0 = new DigitalVideoDisc("The Lion King");
//		dvd0.setCategory("Animation");
//		dvd0.setCost(19.95f);
//		dvd0.setDirector("Roger Allers");
//		dvd0.setLength(87);
//		anOrder1.addMedia(dvd0);
//
//		
//		DigitalVideoDisc dvd1 = new DigitalVideoDisc("Star Wars");
//		dvd1.setCategory("Science Fiction");
//		dvd1.setCost(24.95f);
//		dvd1.setDirector("George Lucas");
//		dvd1.setLength(124);
//		anOrder1.addMedia(dvd1);
//		
//		DigitalVideoDisc dvd2 = new DigitalVideoDisc("Aladdin");
//		dvd2.setCategory("Animation");
//		dvd2.setCost(18.99f);
//		dvd2.setDirector("John Musker");
//		dvd2.setLength(90);
//		anOrder1.addMedia(dvd2);
//		
//		anOrder1.printOrder();
//		////anOrder.addDigitalVideoDisc(dvd); 		//test for adding list parameter
//		////anOrder.addDigitalVideoDisc(dvd0, dvd1);  // test for adding 2 discs parameters
//		////System.out.print("Total Cost is: ");
//		////System.out.println(anOrder.totalCost());
//		
//		
//		
//		Order anOrder2 = new Order();
//		DigitalVideoDisc dvd3 = new DigitalVideoDisc("Moon Knight");
//		dvd3.setCategory("Action");
//		dvd3.setCost(20.2f);
//		dvd3.setDirector("Mohamed Diab");
//		dvd3.setLength(90);
//		anOrder2.addMedia(dvd3);
//
//		DigitalVideoDisc dvd4 = new DigitalVideoDisc("Modern Family");
//		dvd4.setCategory("Sitcom");
//		dvd4.setCost(15.6f);
//		dvd4.setDirector("Becky Mann");
//		dvd4.setLength(20);
//		anOrder2.addMedia(dvd4);
//		
//		anOrder2.printOrder();
	}
}

