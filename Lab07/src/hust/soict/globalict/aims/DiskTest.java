package hust.soict.globalict.aims;
import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.order.Order;

public class DiskTest {
	public static void main(String[] args) {
		DigitalVideoDisc disc = new DigitalVideoDisc("Harry Potter  bbb");
		System.out.println(disc.search("abc   HARRY    "));	
		Order anOrder1 = new Order();
		//DigitalVideoDisc[] dvd = new DigitalVideoDisc[3]; //test for adding list parameter
		
		DigitalVideoDisc dvd0 = new DigitalVideoDisc("The Lion King");
		dvd0.setCategory("Animation");
		dvd0.setCost(19.95f);
		dvd0.setDirector("Roger Allers");
		dvd0.setLength(87);
		anOrder1.addDigitalVideoDisc(dvd0);

		
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("Star Wars");
		dvd1.setCategory("Science Fiction");
		dvd1.setCost(24.95f);
		dvd1.setDirector("George Lucas");
		dvd1.setLength(124);
		anOrder1.addDigitalVideoDisc(dvd1);
		
		
		DigitalVideoDisc dvd2 = new DigitalVideoDisc("Aladdin");
		dvd2.setCategory("Animation");
		dvd2.setCost(18.99f);
		dvd2.setDirector("John Musker");
		dvd2.setLength(90);
		anOrder1.addDigitalVideoDisc(dvd2);
		
		anOrder1.printOrder();
		anOrder1.getALuckyItem();
		anOrder1.printOrder();

	}
}
