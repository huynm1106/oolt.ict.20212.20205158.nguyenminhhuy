package hust.soict.globalict.aims.utils;
public class DateUtils {
	public static void dateCompare(MyDate date1, MyDate date2) {
		if(isBefore(date1, date2) == 1) {
			date1.printDate();
			System.out.print(" is before ");
			date2.printDate();
		}
		else if(isBefore(date1, date2) == -1) {
			date1.printDate();
			System.out.print(" is after ");
			date2.printDate();
		}
		else{
			date1.printDate();
			System.out.print(" is the same as ");
			date2.printDate();
		}
		}
	
	public static void dateSort(MyDate... date) {
		for (int i = 0; i < date.length; i++) {
			for (int j = i; j < date.length; j++) {
				if (isBefore(date[j], date[i]) == 1) {
					MyDate temp = date[i];
					date[i] = date[j];
					date[j] = temp;
				}
			}
		}
		System.out.println("\nSorting the dates in ascending order: ");
		for(int i = 0; i < date.length; i++) {
			date[i].printDate();
			System.out.print("\n");
		}
	}
	public static int isBefore(MyDate date1, MyDate date2) {
		if(date1.getYear() < date2.getYear()) {
			return 1;
		}
		else if(date1.getYear() > date2.getYear()) {
			return -1;
		}
		else {
			if(date1.getMonth() < date2.getMonth()) {
				return 1;
			}
			else if(date1.getMonth() > date2.getMonth()) {
				return -1;
			}
			else {
				if(date1.getDay() < date2.getDay()) {
					return 1;
				}
				else if(date1.getDay() > date2.getDay()) {
					return -1;
}
				else {
					return 0;
				}
			}
	}
	}
}

