package hust.soict.globalict.aims.order;
import java.lang.Math;
import java.util.ArrayList;

import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.media.Media;
import hust.soict.globalict.aims.utils.MyDate;


public class Order {

	public static final int MAX_NUMBERS_ORDERED = 10;    	
	//int qtyOrdered = 0;   // nbOrders
	//private DigitalVideoDisc itemOrdered[] = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
	private ArrayList<Media> itemsOrdered = new ArrayList<Media>();
	public static final int MAX_LIMITTED_ORDERS = 5;
	private static int nbOrders = 0;
	private MyDate dateOrdered;
	
	
	public MyDate getDateOrdered() {
		return dateOrdered;
	}
	public void setDateOrdered(MyDate dateOrdered) {
		this.dateOrdered = dateOrdered;
	}

	public Order() {
		MyDate date = new MyDate();		///cac order cung duoc tao trong 1 ngay
		setDateOrdered(date);
		if(nbOrders<MAX_LIMITTED_ORDERS)
			nbOrders++;
		else 
			System.out.println("The order is full");
	}
	
	public void addMedia(Media ...media) {
		for (Media m : media) {			// input 1 array ten la media, kieu Media, duyet tat ca cac phan tu m trong media
            if(itemsOrdered.contains(m)){
                System.out.println("Media " + m.getTitle() + " is existed!");
            }
            else if(itemsOrdered.size()>= MAX_NUMBERS_ORDERED){
                System.out.println("Max number of medias reached!");
                return;
            }
            else{
                itemsOrdered.add(m);
                System.out.println("Media " + m.getTitle() + " is added");
            }
        }
	}
	
	public void removeMedia(Media ...media) {
		if(itemsOrdered.size() == 0)
			System.out.println("Empty order");
		else
			for(Media m : media) {
				if(itemsOrdered.contains(m)){
	                System.out.println("Media " + m.getTitle() + " is existed!");
	            }
			}
	}
	
	public void removeMedia(int id,DigitalVideoDisc... dvd) {
		if (dvd.length == 0){
			System.out.println("Your order has no items to be removed.");
			return;
		}
		for (DigitalVideoDisc m:dvd) {
            if (m.getId() == id) {
            	m.setCategory(null);
            	m.setTitle(null);
            	m.setCost(0);
            	m.setId(0);
            	m.setDirector(null);
            	m.setLength(0);
            }
        }
            System.out.println("Media removed successfully!");
        }
	
	
	public float totalCost() {
		float sum = 0;
		for(Media media : itemsOrdered) {
			sum += media.getCost();
		} 
		return sum;
	}
	
	public void printOrder() {
		//float totalCost = 0;
		System.out.println("***********************Order***********************");
		System.out.print("Date: ");  
		dateOrdered.printDate();
		System.out.print("\n");
		System.out.println("Ordered Items:");
		for(int i = 0 ; i < itemsOrdered.size(); i++) {
			System.out.println((i+1) + ". DVD - " + itemsOrdered.get(i).getTitle() + " - " 
					+ itemsOrdered.get(i).getCategory() + " - " 
					//+ itemsOrdered.get(i).getDirector() + " - " 
					//+ itemsOrdered.get(i).getLength() + ": " 
					+ itemsOrdered.get(i).getCost() + "$");
		}
		System.out.println("Total cost: " + totalCost());
		System.out.println("***************************************************");
	}
	
	public Media getALuckyItem() {
        int rand = (int)(Math.random() * itemsOrdered.size());
        Media lucky = itemsOrdered.get(rand);
        itemsOrdered.get(rand).setCost(0);
        System.out.println("Congratulation! You have 1 lucky item for free: item " + (rand+1));
        return lucky;
	}

}

//kieu add 1
//public void addDigitalVideoDisc(DigitalVideoDisc disc) {
//	if(qtyOrdered >= MAX_NUMBERS_ORDERED){
//		System.out.println("The order is full");
//	}
//	else {
//		itemOrdered[qtyOrdered] = disc;
//		//System.out.println("The disc " + (qtyOrdered + 1) + " has been added");
//		qtyOrdered++;
//	}
//}		
///// kieu add 2
//public void addDigitalVideoDisc(DigitalVideoDisc [] dvdList) {
//	if(qtyOrdered >= MAX_NUMBERS_ORDERED){
//		System.out.println("The order is full");
//	}
//	else if(qtyOrdered >= (MAX_NUMBERS_ORDERED - dvdList.length)) {
//		System.out.println("Not enough space for all the discs input");
//	}
//	else {
//		for(int i = 0; i < dvdList.length; i++) {
//			itemOrdered[qtyOrdered] = dvdList[i];
//			System.out.println("The disc " + (qtyOrdered + 1) + " has been added");
//			qtyOrdered++;
//		}
//	}
//}		
/////kieu add 3
//public void addDigitalVideoDisc(DigitalVideoDisc dvd1,DigitalVideoDisc dvd2) {
//	if(qtyOrdered >= MAX_NUMBERS_ORDERED){
//		System.out.println("The order is full");
//	}
//	else if(qtyOrdered >= (MAX_NUMBERS_ORDERED - 2)) {
//		System.out.println("Not enough space for all the discs input");
//	}
//	else {
//			itemOrdered[qtyOrdered] = dvd1;
//			System.out.println("The disc " + (qtyOrdered + 1) + " has been added");
//			qtyOrdered++;
//			itemOrdered[qtyOrdered] = dvd2;
//			System.out.println("The disc " + (qtyOrdered + 1) + " has been added");
//			qtyOrdered++;
//	}
//}			


//public void removeDigitalVideoDisc(DigitalVideoDisc disc) {
//	if(qtyOrdered == 0)
//		System.out.println("Empty order");
//	else {
//		for(int i = 0; i < qtyOrdered; i++) {
//			if(itemOrdered[i] == disc)
//				itemOrdered[i] = itemOrdered[i + 1];
//		}
//		System.out.println("Remove disc " + (qtyOrdered) + " succesfully!");
//		qtyOrdered--;
//	}
//}
