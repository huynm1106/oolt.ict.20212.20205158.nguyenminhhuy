public class Order {

	public static final int MAX_NUMBERS_ORDERED = 10;    	
	int qtyOrdered = 0;   // nbOrders
	private DigitalVideoDisc itemOrdered[] = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
	public static final int MAX_LIMITTED_ORDERS = 5;
	private static int nbOrders = 0;
	private MyDate dateOrdered;
	
	
	public MyDate getDateOrdered() {
		return dateOrdered;
	}
	public void setDateOrdered(MyDate dateOrdered) {
		this.dateOrdered = dateOrdered;
	}

	public Order() {
		MyDate date = new MyDate();		///cac order cung duoc tao trong 1 ngay
		setDateOrdered(date);
		if(nbOrders<MAX_LIMITTED_ORDERS)
			nbOrders++;
		else 
			System.out.println("The order is full");
	}

	///kieu add 1
	public void addDigitalVideoDisc(DigitalVideoDisc disc) {
		if(qtyOrdered >= MAX_NUMBERS_ORDERED){
			System.out.println("The order is full");
		}
		else {
			itemOrdered[qtyOrdered] = disc;
			//System.out.println("The disc " + (qtyOrdered + 1) + " has been added");
			qtyOrdered++;
		}
	}		
	/// kieu add 2
	public void addDigitalVideoDisc(DigitalVideoDisc [] dvdList) {
		if(qtyOrdered >= MAX_NUMBERS_ORDERED){
			System.out.println("The order is full");
		}
		else if(qtyOrdered >= (MAX_NUMBERS_ORDERED - dvdList.length)) {
			System.out.println("Not enough space for all the discs input");
		}
		else {
			for(int i = 0; i < dvdList.length; i++) {
				itemOrdered[qtyOrdered] = dvdList[i];
				System.out.println("The disc " + (qtyOrdered + 1) + " has been added");
				qtyOrdered++;
			}
		}
	}		
	///kieu add 3
	public void addDigitalVideoDisc(DigitalVideoDisc dvd1,DigitalVideoDisc dvd2) {
		if(qtyOrdered >= MAX_NUMBERS_ORDERED){
			System.out.println("The order is full");
		}
		else if(qtyOrdered >= (MAX_NUMBERS_ORDERED - 2)) {
			System.out.println("Not enough space for all the discs input");
		}
		else {
				itemOrdered[qtyOrdered] = dvd1;
				System.out.println("The disc " + (qtyOrdered + 1) + " has been added");
				qtyOrdered++;
				itemOrdered[qtyOrdered] = dvd2;
				System.out.println("The disc " + (qtyOrdered + 1) + " has been added");
				qtyOrdered++;
		}
	}			
	
	
	public void removeDigitalVideoDisc(DigitalVideoDisc disc) {
		if(qtyOrdered == 0)
			System.out.println("Empty order");
		else {
			for(int i = 0; i < qtyOrdered; i++) {
				if(itemOrdered[i] == disc)
					itemOrdered[i] = itemOrdered[i + 1];
			}
			System.out.println("Remove disc " + (qtyOrdered) + " succesfully!");
			qtyOrdered--;
		}
	}
	
	
	
	public float totalCost() {
		float sum = 0;
		for(int i = 0; i < qtyOrdered; i++) {
			sum += itemOrdered[i].getCost(); 
		} 
		return sum;
	}
	
	public void printOrder() {
		float totalCost = 0;
		System.out.println("***********************Order***********************");
		System.out.print("Date: ");  
		dateOrdered.printDate();
		System.out.println("Ordered Items:");
		for(int i = 0 ; i < qtyOrdered; i++) {
			totalCost += itemOrdered[i].getCost();
			System.out.println((i+1) + ". DVD - " + itemOrdered[i].getTitle() + " - " 
					+ itemOrdered[i].getCategory() + " - " + itemOrdered[i].getDirector() + " - " 
					+ itemOrdered[i].getLength() + ": " + itemOrdered[i].getCost() + "$");
		}
		System.out.println("Total cost: " + totalCost);
		System.out.println("***************************************************");
	}
}
