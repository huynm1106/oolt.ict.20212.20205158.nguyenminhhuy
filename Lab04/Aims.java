
public class Aims {
	public static void  main(String[] args) {
		
		Order anOrder1 = new Order();
		//DigitalVideoDisc[] dvd = new DigitalVideoDisc[3]; //test for adding list parameter
		
		DigitalVideoDisc dvd0 = new DigitalVideoDisc("The Lion King");
		dvd0.setCategory("Animation");
		dvd0.setCost(19.95f);
		dvd0.setDirector("Roger Allers");
		dvd0.setLength(87);
		anOrder1.addDigitalVideoDisc(dvd0);

		
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("Star Wars");
		dvd1.setCategory("Science Fiction");
		dvd1.setCost(24.95f);
		dvd1.setDirector("George Lucas");
		dvd1.setLength(124);
		anOrder1.addDigitalVideoDisc(dvd1);
		
		DigitalVideoDisc dvd2 = new DigitalVideoDisc("Aladdin");
		dvd2.setCategory("Animation");
		dvd2.setCost(18.99f);
		dvd2.setDirector("John Musker");
		dvd2.setLength(90);
		anOrder1.addDigitalVideoDisc(dvd2);
		
		anOrder1.printOrder();
		//anOrder.addDigitalVideoDisc(dvd); 		//test for adding list parameter
		//anOrder.addDigitalVideoDisc(dvd0, dvd1);  // test for adding 2 discs parameters
		//System.out.print("Total Cost is: ");
		//System.out.println(anOrder.totalCost());
		
		
		
		Order anOrder2 = new Order();
		DigitalVideoDisc dvd3 = new DigitalVideoDisc("Moon Knight");
		dvd3.setCategory("Action");
		dvd3.setCost(20.2f);
		dvd3.setDirector("Mohamed Diab");
		dvd3.setLength(90);
		anOrder2.addDigitalVideoDisc(dvd3);

		DigitalVideoDisc dvd4 = new DigitalVideoDisc("Modern Family");
		dvd4.setCategory("Sitcom");
		dvd4.setCost(15.6f);
		dvd4.setDirector("Becky Mann");
		dvd4.setLength(20);
		anOrder2.addDigitalVideoDisc(dvd4);
		
		anOrder2.printOrder();
	}
}
