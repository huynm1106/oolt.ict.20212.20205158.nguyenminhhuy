import java.util.Scanner;
import java.time.format.DateTimeFormatter;  
import java.time.LocalDate;
import java.util.Locale;

public class MyDate {
	private int day;
	private int month;
	private int year;
	
	public int getDay() {
		return day;
	}
	public void setDay(int day) {
		if(0 < day && day < 32)
			this.day = day;
		else
			System.out.println("Invalid day");
		
	}
	public int getMonth() {
		return month;
	}
	public void setMonth(int month) {
		if(0 < month && month < 13)
			this.month = month;
		else
			System.out.println("Invalid month");
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		if(year > 0)
			this.year = year;
		else
			System.out.println("Invalid year");
	}
	
	
	public MyDate() {
		LocalDate current = LocalDate.now();
		this.day= current.getDayOfMonth();
		this.month= current.getMonthValue();
		this.year= current.getYear();
	}
	public MyDate(int day, int month, int year) {
		super();
		this.day = day;
		this.month = month;
		this.year = year;
	}
	public MyDate(String day, String month, String year) {
		
	}
	public MyDate(String date){
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMMM d yyyy", Locale.ENGLISH);
		LocalDate current_date = LocalDate.parse(date, formatter);
		this.day= current_date.getDayOfMonth();
		this.month= current_date.getMonthValue();
		this.year= current_date.getYear();
		}      
	
	
	public MyDate accept() {
		Scanner sc= new Scanner(System.in);
		System.out.println("Enter a date:");
		String date_string =  sc.nextLine();
		MyDate date = new MyDate(date_string);
		return date;
	}
	
	public void print() {
		String monthStr = null, dayStr = null;
		if(month == 1) 
			monthStr = "January";
		else if(month == 2)
			monthStr = "February";
		else if(month == 3)
			monthStr = "March";
		else if(month == 4)
			monthStr = "April";
		else if(month == 5)
			monthStr = "May";
		else if(month == 6)
			monthStr = "June";
		else if(month == 7)
			monthStr = "July";
		else if(month == 8)
			monthStr = "August";
		else if(month == 9)
			monthStr = "September";
		else if(month == 10)
			monthStr = "October";
		else if(month == 11)
			monthStr = "November";
		else if(month == 12)
			monthStr = "December";
		
		if(day == 1) 
			dayStr = Integer.toString(day) + "st";
		else if(day == 2) 
			dayStr = Integer.toString(day) + "nd";
		else if(day == 3) 
			dayStr = Integer.toString(day) + "rd";
		else
			dayStr = Integer.toString(day) + "th";
		System.out.println(monthStr + " " + dayStr + " " + year);
	}
	
	public void printline() {
		System.out.println("The date is " + day + "/" + month + "/" + year);
	}
	
	public void printDate() {
		System.out.print(day + "/" + month + "/" + year);
	}

	public void printFormat() {
		String monthStr = null;
		if(month == 1) 
			monthStr = "Jan";
		else if(month == 2)
			monthStr = "Feb";
		else if(month == 3)
			monthStr = "Mar";
		else if(month == 4)
			monthStr = "Apr";
		else if(month == 5)
			monthStr = "May";
		else if(month == 6)
			monthStr = "Jun";
		else if(month == 7)
			monthStr = "Jul";
		else if(month == 8)
			monthStr = "Aug";
		else if(month == 9)
			monthStr = "Sep";
		else if(month == 10)
			monthStr = "Oct";
		else if(month == 11)
			monthStr = "Nov";
		else if(month == 12)
			monthStr = "Dec";
		if(day < 10) {
		System.out.println("0" + day + "-" + monthStr +"-"+ year);
		}else {
		System.out.println(day + "-" + monthStr +"-"+ year);
		}
		}
}
